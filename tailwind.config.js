/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
  ],
  theme: {
    extend: {
      colors : {
        'teal-teal-300':  'var(--Teal-Teal-300, #4FD1C5)',
        'gray-gray-700': 'var(--Gray-Gray-700, #2D3748)',
        'gray-gray-400': 'var(--Gray-Gray-400, #A0AEC0)',
        'gray-gray-500': 'var(--Gray-Gray-500, #718096)',
        'second-gray': 'var(--second-text, #A0AEC0)',
        
      },
      borderRadius : {
        c8: '8px',
        c15: '15px',
        c12: '12px',
      },

      gap: {
        "c-14": "14px",
        "c-20": "20px",
        "c-24": "24px",
      },
      backgroundColor : {
        'teal-teal-300':  'var(--Teal-Teal-300, #4FD1C5)',
        "Very-Light-Grey": "#CDCDCD",
        "Gray-Gray-300" : "#CBD5E0",
      },
      fontFamily: {
        helvetica: 'Helvetica, Arial, sans-serif',
      },
    },
  },
  plugins: [],
}